'''Builds bash commands
'''
from subprocess import run, CompletedProcess
from time import sleep

class Command:
    '''Bash command with some extra settings
    '''
    def __init__(self, command, skip_key, settings, cwd=None):
        self.command = command
        self.skip_key = skip_key
        self.skip = self.skip_key in settings.skip.value
        self.cwd = cwd

    def run_command(self, check=True):
        '''Run self and return status.
        Calls exit if set to skip.
        '''

        if not self.skip:
            if self.cwd is not None:
                self.cwd = str(self.cwd)
            return run(self.command, shell=True, check=check, cwd=self.cwd)
        return run('exit', shell=True)

class PyCommand(Command):
    '''Python command that thinks it's a bash command.
    Arguments for command can be passed through kwargs.
    Command should return a CompletedProcess object.
    '''
    def __init__(self, command, skip_key, settings, cwd=None, **kwargs):
        super().__init__(command, skip_key, settings, cwd)
        self.kwargs = kwargs

    def run_command(self, check=True):
        if not self.skip:
            return self.command(**self.kwargs)
        return run('exit', shell=True)

def wait_for_usb(usb_id):
    print('Waiting for USB device {} to disconnect...'.format(usb_id))
    while True:
        if run('lsusb | grep {}'.format(usb_id), shell=True).returncode == 0:
            # Returncode will be non-0 if grep returns empty, i.e. disconnected
            sleep(1)
            continue
        else:
            sleep(3)
            # Give device a few seconds to connect when entering programming mode
            break
    return CompletedProcess(usb_id, 0)

def make_commands(settings):
    '''Gives a list of Command objects used to make and flash.
    '''

    return [
        Command(
            'make clean',
            'c',
            settings,
            '{}/keyboards/{}'.format(settings.path.value, settings.keyboard.value)
        ),
        Command(
            'make {}'.format(settings.layout.value),
            'm',
            settings,
            '{}/keyboards/{}'.format(settings.path.value, settings.keyboard.value)
        ),
        PyCommand(
            wait_for_usb,
            'w',
            settings,
            None,
            usb_id=settings.identifier.value
        ),
        Command(
            'dfu-programmer {} erase'.format(settings.mcu.value),
            'e',
            settings,
            None
        ),
        Command(
            'dfu-programmer {} flash {}'.format(
                settings.mcu.value,
                settings.keyboard.value + '_' + settings.layout.value + '.hex'
            ),
            'f',
            settings,
            settings.path.value
        ),
        Command(
            'dfu-programmer {} reset'.format(settings.mcu.value),
            'r',
            settings,
            None
        )
    ]
