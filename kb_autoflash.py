#!/usr/bin/env python3.5
'''Utility for automatically flashing keyboard firmware using QMK.
'''

from subprocess import run

# Local packages
import parser
import settings as settings_module
import dotfile
import caller

def main():
    '''Main function for when calling from CLI.
    '''

    warnings = []
    parse_res = parser.make_parser().parse_args()
    dotfile_conf = dotfile.config  # Grab our dotfile info for settings
    settings = settings_module.make_settings(parse_res, dotfile_conf)

    usb_validity = verify_usb(settings.identifier.value)
    if not usb_validity:
        warnings.append(
            'USB identifier not found in lsusb! Make sure your keyboard\'s ID shows '
            'up as {} in lsusb, invoke kb_autoflash with e.g. -i 1337:babe. The program '
            'can still run but will not handle waiting. It is recommended that you use the '
            '--skip option to either only compile or only flash.'
            .format(settings.identifier.value)
        )

    calls = [c for c in caller.make_commands(settings) if not c.skip]

    print('\nSettings:')
    for key, val in settings._asdict().items():
        print('  {: <10} - {}'.format(key, val.value))

    print('\nCommands:')
    for call in calls:
        print('  ' + str(call.command))

    print('\nWarnings:')
    for warning in warnings:
        print('  * ' + warning)

    if not input_bool('\nProceed?', not bool(warnings)):
        # Empty (no) warnings -> false; invert and use as default.
        exit()

    for call in calls:
        res = call.run_command()

def verify_usb(identifier):
    '''Check whether identifier can be found in lsusb.
    Will match if any line contains identifier, so not
    quite safe.
    '''

    return run('lsusb | grep {}'.format(identifier), shell=True).returncode == 0

def input_bool(prompt='', default=None):
    '''Take y/n input from user. Supports defaults.
    '''

    prompt_appendage = ' [y/n]\n'
    if default is True:
        prompt_appendage = ' [Y/n]\n'
    elif default is False:
        prompt_appendage = ' [y/N]\n'

    while True:
        answer = input(prompt + prompt_appendage)
        if answer == '' and default is not None:
            return default
        elif answer.lower() == 'y':
            return True
        elif answer.lower() == 'n':
            return False
        else:
            continue

if __name__ == '__main__':
    main()
    print('\nProgram done; exiting.')
    exit()
