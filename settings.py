'''Each setting has several layers, with lowest index having the highest priority.
  0. CLI arguments
  1. ~/.kb_autoflash
  2. Environment variables
  3. Values read from files
Some of these may not be implemented. For example, keyboard and layout are read from
CLI arguments and must be provided explicitly as a security measure.
'''

from collections import namedtuple
from os import environ
from pathlib import Path
import patterns

class Setting:
    '''Essentially a list, but handles issues and abstracts representation.
    '''

    def __init__(self, values=tuple([None]*4)):
        if len(list(values)) == 4:
            self.values = list(values)
        else:
            raise ValueError(
                str(values)
                + ' passed to SettingValue. '
                'value must be list(-like) with length 4. Use None for unassigned values.'
            )


    def __bool__(self):
        return self.value != None

    @property
    def value(self):
        '''Returns the first non-None value of its contents.
        '''

        for val in self.values:
            if val is not None:
                return val
        return None

    def verify_n(self, fnc, i):
        '''Returns result of fnc for i:th value of Setting.
        '''
        return fnc(self.values[i])

    def verify(self, fnc):
        '''Returns result of fnc for first non-None value of Setting.
        '''
        return fnc(self)

    def verify_all(self, fnc):
        '''Gives a list of verification results for
        all values in setting.
        '''
        return [fnc(i) for i in self.values]

def make_settings(parse_res, dotfile_conf):
    '''Combine implicit and explicit settings and
    check for errors and issues.
    '''

    print('Reading settings...')
    settings = namedtuple('settings', [
        'keyboard',
        'layout',
        'identifier',
        'mcu',
        'skip',
        'path',
    ])

    keyboard = Setting([parse_res.keyboard, None, None, None])
    layout = Setting([parse_res.layout, None, None, None])
    path = Setting([
        parse_res.path,
        dotfile_conf.get('path'),
        environ['QMK_PATH'],
        None
    ])
    identifier = Setting([
        parse_res.identifier,
        dotfile_conf.get('identifier'),
        None,
        make_identifier(path.value, keyboard.value, layout.value)
    ])
    mcu = Setting([
        parse_res.mcu,
        dotfile_conf.get('mcu'),
        None,
        make_mcu(path.value, keyboard.value)
    ])
    skip = Setting([
        parse_res.skip,
        dotfile_conf.get('skip'),
        None,
        None
    ])

    return settings(
        keyboard,
        layout,
        identifier,
        mcu,
        skip,
        path
    )

def make_identifier(path, keyboard, layout):
    '''Get USB identifier from a config.h file.
    '''

    path_templates = [
        '{path}/keyboards/{keyboard}/keymaps/{layout}/config.h',
        '{path}/keyboards/{keyboard}/config.h'
    ]
    paths = [p.format(path=path, keyboard=keyboard, layout=layout) for p in path_templates]

    for path_str in paths:
        if Path(path_str).exists():
            defs = get_c_defines(path_str)
            vendor = patterns.hexword.match(defs['VENDOR_ID']).group(1)
            product = patterns.hexword.match(defs['PRODUCT_ID']).group(1)
            identifier = '{}:{}'.format(vendor, product).lower()
            if patterns.identifier.match(identifier):
                return identifier
            else:
                message = (
                    'ID from {path_str} could not be read properly, '
                    'gave {identifier} which does not match the NNNN:NNNN '
                    'pattern where N is a hexadecimal digit.'
                )
                raise Exception(message.format(path_str=path_str, identifier=identifier))

    raise Exception('No config.h found at any of: {}'.format(paths))

def make_mcu(path, keyboard):
    '''Retreive MCU value from .mk file.
    '''

    file_path = '{path}/keyboards/{keyboard}/rules.mk'.format(path=path, keyboard=keyboard)

    with open(str(file_path), 'r') as file_obj:
        return patterns.mcu.search(file_obj.read()).group(1)

def get_c_defines(file_path):
    '''Retreive #define macros from a file as a dict.
    '''

    defs = {}
    with open(str(file_path), 'r') as file_obj:
        pattern = patterns.define
        for i, line in enumerate(file_obj):
            match = pattern.match(line)
            if match:
                if match.group(1) not in defs:
                    defs[match.group(1)] = match.group(2)
                else:
                    raise Exception(  # This is probably not the best kind of exception.
                        'Tried to read #DEFINE {key} from {fname} twice!\n'
                        'Initial value: {init_val}\n'
                        'Offending value: {off_val}\n'
                        'at line {l}'.format(
                            key=match.group(2),
                            fname=str(file_path),
                            init_val=defs[match.group(2)],
                            off_val=match.group(3),
                            l=i
                        )
                    )
    return defs
