'''Regex patterns for use in KB Autoflash
'''

import re

define = re.compile(r'''\#define    # #define literal
                        \s+         # space, may be more than one for formatting
                        (\w+)       # key
                        \s+         # space, may be more than one for formatting
                        (\w+)       # value
                        \s*         # optional space before EOL; also covers trailing whitespace. 
                        (?:\/\/.+)* # comments
                        \n''', re.X)# EOL

mcu = re.compile(r'^MCU = (\w*)', re.M)
# Pretty sloppy, but we only need to grab the MCU name.
# Just makes sure to not grab from a comment.

hexword = re.compile(r'0x([\da-fA-F]{4})')
# Matches any value written 0xNNNN for hexadecimal digits N.
# Requires four digits.

identifier = re.compile(r'([\da-fA-F]){4}:([\da-fA-F]{4})')
# Matches a sequence NNNN:NNNN for hexadecimal digits N.
# Requires four digits on both sides of a colon.
