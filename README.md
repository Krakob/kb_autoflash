# Keyboard Autoflash

Utility for flashing keyboard with QMK firmware! Just write your firmware and it will handle every step between `make` and `reset`. All you need to do is put the keyboard in programming mode when prompted.

Only supports Linux (may work for Mac; untested).

## Installation

* Download or clone the repository.
* Make sure `kb_autoflash.py` is executable (`chmod +x kb_autoflash.py`)
* Make a config file in your home directory, `~/.kb_autoflash.conf`. See dotfile section for template.
* Optionally, put a symbolic link to `kb_autoflash.py` in e.g. `/usr/bin` or another directory in your `PATH`.

This program is developed on Python 3.5. Future Python versions should work, and past ones may as well. No modules outside the standard library are currently needed.

Note that you'll need to be able to use dfu-programmer as a non-root user. See [tmk FAQ](https://github.com/tmk/tmk_keyboard/wiki/FAQ-Build#linux-udev-rules)

`pip` support is planned but not yet available.

## Usage

Call the program from the command line, supplying your keyboard and layout as the first two arguments, e.g. `kb_autoflash.py gh60 default` in order to build and flash a GH60 keyboard with its default layout.

## Config template
The program will only read the first section, regardless of name.

It is not recommended that you assign mcu or identifier, as these can be read automatically from your project files. They can also be provided with the `--identifier` and `--mcu` options, respectively.

    [section]
    path = /home/alice/git/qmk_firmware
    skip = c
    # identifier = feed:6060
    # mcu = atmega32u4

## Skipping steps
You can skip parts certain steps of the process. For example, passing `-s c` will skip `make clean`, or you can pass `-s we` to skip waiting for the device to go into programming mode and erasing its contents. These skips are supported:

    c - make clean
    m - make {layout}
    w - wait for keyboard to disappear from lsusb
    e - dfu-programmer {mcu} erase
    f - dfu-programmer {mcu} flash {layout}
    r - dfu-programmer {mcu} reset

## Contributing
Development primarily hosted on [GitLab](https://gitlab.com/Krakob/kb_autoflash). Please submit any issues or pull requests there.

The program runs on Python 3.5.x and currently only makes use of the standard library.

## License
GPL. See `LICENSE.md`.
