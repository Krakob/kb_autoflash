import argparse

from os import environ
from pathlib import Path

def make_parser():
    '''Define the parser and parse, as parsers do!
    '''

    print('Parsing command line options...')

    parser = argparse.ArgumentParser(
        description=(
            'Automatically flash your keyboard firmware. '
            'Does everything between compiling your firmware '
            'and resetting your board. The only thing you need to do '
            'is put the board in reset mode when prompted!'
        )
    )

    parser.add_argument(
        'keyboard',
        help='name of the keyboard in the qmk/keyboards '
             'folder, e.g. planck or atreus62'
    )
    parser.add_argument(
        'layout',
        help='name of the keyboard layout to use in '
             'the keymaps folder, e.g. default or dvorak'
    )
    parser.add_argument(
        '-p, --path',
        metavar='path',
        dest='path',
        type=Path,
        default=environ['QMK_PATH'],
        help='Path of QMK. Set environment variable QMK_PATH and you can skip this.'
    )
    parser.add_argument(
        '-i, --identifier',
        metavar='identifier',
        dest='identifier',
        help='ID of your keyboard. Can be found with lsusb. Normally supplied '
             'by config.h from the keyboard\'s settings but may need to set '
             'manually on your first run as factory defaults might not match '
             'QMK\'s settings, or if your firmware will use its own.'
    )
    parser.add_argument(
        '-s, --skip',
        metavar='skip',
        dest='skip',
        default='',
        help='Give letters to skip corresponding step in the process'
    )
    parser.add_argument(
        '-m, --mcu',
        metavar='mcu',
        dest='mcu',
        default=None,
        help='ONLY USE IF YOU KNOW WHAT YOU ARE DOING. Override MCU from rules.mk'
    )

    return parser
