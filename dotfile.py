import configparser
from pathlib import Path

path = Path('~/.kb_autoflash.conf').expanduser()

config = {}

if path.exists():
    parse = configparser.ConfigParser()
    parse.read(str(path))
    section = parse.sections()[0]
    config = dict(parse[section])
